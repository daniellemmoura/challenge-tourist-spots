document.addEventListener('DOMContentLoaded', function() {
    const list = [];

    let form = document.querySelector(".tourist-spots-form");
    let titleInput = document.querySelector(".tourist-spots-input-title");
    



    form.addEventListener('submit', addItemToList);

    function addItemToList(event) {
        event.preventDefault();

        let divResult = document.querySelector(".tourist-spots-result");
        let divResultTitle = document.createElement("h2");
        divResultTitle = document.querySelector(".tourist-spots-input-title").value;
        
        //console.log(titleValue);

        if (divResultTitle != "") {
            let title = {
                name: divResultTitle
            };
        }

        list.push(divResultTitle);
        renderListItems();
        resetInputs();
}

function renderListItems() {
    let titleTest = "";

    list.forEach(function (title) {
        titleTest += `
                <div class="tourist-spots-result">
                <h2>${title.name}</h2>
                </div>
        `;
    });

    titleInput.innerHTML = titleTest;
}

function resetInputs() {
    titleInput.value = "";
}


});